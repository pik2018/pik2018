Projekt semestralny z przedmiotu PIK

Michał Smoła, Marcin Hałas, Kuba Jałowiec

Sciaganie do IntelliJ (dziala w wersji IntelliJ IDEA 2018.1.2 (Community Edition)

1. File -> New -> Project from Version Control -> Git i ustawiamy adres: https://bitbucket.org/pik2018/borowik.git oraz wybieramy folder, do ktorego chcemy zaladowac projekt.
2. Run -> Edit Configurations... -> klikamy na zielony plus po lewej stronie -> Gradle -> Gradle project (rozwijamy przez [...]) -> wybieramy folder, w ktorym jest sciagniety projekt
3. File -> Project structure -> Project SDK -> wybieramy co chcemy 

Stos technologiczny:

- Repozytorium kodu:
	- Bitbucket
- Śledzenie błędów / zarządzanie projektem:
	- JIRA
- Budowanie i deployment:
	- Jenkins
- Konfiguracja projektu:
	- Gradle
- Serwer:
	- Tomcat
	

WEiTI PW 2018